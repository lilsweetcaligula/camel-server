let rec respond ~(status : Http_status.t)
                ?(headers : Headers.t = Headers.empty ())
                ?(body : bytes option)
                ?(body_len : int = 0)
                ?(http_ver : string = "HTTP/1.1")
                (client : Unix.file_descr) : unit =

  let req_line = [http_ver;
                  Http_status.to_code_string status;
                  Http_status.reason status] |> String.concat " "

  in
  let response, response_len = match body with
  | None ->
    let response = Bytes.of_string (req_line ^ "\r\n" ^ (Headers.to_string headers)) in
    let response_len = Bytes.length response in
    response, response_len

  | Some body ->
    let headers = Headers.add_header "Content-Length"
                                     (string_of_int body_len)
                                     headers

    in
    let body_buf = Bytes.create body_len in
    Bytes.blit body 0 body_buf 0 body_len;

    let response = [req_line; Headers.to_string headers; ""]
                   |> List.map Bytes.of_string
                   |> Bytes.concat (Bytes.of_string "\r\n")
                   |> (Fun.flip Bytes.cat) body_buf
    in
    let response_len = Bytes.length response

    in response, response_len

  in ignore (Unix.write client response 0 response_len)


and read_file (path : string) : bytes * int =
  let kBytesPerRead = 64 in

  let file = Unix.(openfile path [O_RDONLY] 0)
  and buf = ref (Bytes.create 32)
  and len = ref 0

  in
  let () =
    try
      let done_reading = ref false

      in
      while not !done_reading do
        let required_capa = !len + kBytesPerRead

        in
        if required_capa > Bytes.length !buf then begin
          let new_buf = Bytes.create required_capa in
          Bytes.blit !buf 0 new_buf 0 !len;
          buf := new_buf
        end;

        let nbytes = Unix.read file !buf !len kBytesPerRead

        in
        if nbytes > 0 then
          len := !len + nbytes
        else
          done_reading := true
      done
    with e -> Unix.close file; raise e
  in Unix.close file;

  !buf, !len


and send_file ~(status : Http_status.t)
              ?(headers : Headers.t = Headers.empty ())
              ?(http_ver : string = "HTTP/1.1")
              (client : Unix.file_descr)
              (path : string) : unit =
  let file_content, file_sz = read_file path
  in respond ~status ~headers ~http_ver ~body:file_content ~body_len:file_sz client;

