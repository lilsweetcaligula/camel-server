type t = GET | POST;;

exception InvalidRequestMethodError;;

let of_string = function
| "GET"  -> GET
| "POST" -> POST
| _      -> raise InvalidRequestMethodError

and to_string = function
| GET  -> "GET"
| POST -> "POST";;

