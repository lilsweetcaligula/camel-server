type route_t = Http_req_meth.t * string;;

let matches (request : Http_req.t) (route : route_t) : bool =
  let meth', path' = route
  and { Http_req.meth; Http_req.path; _ } = request

  (* todo: normalization and proper path matching *)
  in meth' = meth && Url.normalize path' = Url.normalize path;;

