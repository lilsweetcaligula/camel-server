type t = {
  meth     : Http_req_meth.t;
  path     : string;
  http_ver : string;
  headers  : Headers.t;
  body     : bytes option
};;

exception MalformedHttpRequestError;;

let kBytesPerRead = 32;;
let kEndOfHeaders = "\r\n\r\n";;

let read (client : Unix.file_descr) : t =
  let buf = ref (Bytes.create kBytesPerRead)
  and offset = ref 0
  and result = ref None in
  let done_reading () = match !result with Some _ -> true | None -> false

  in
  while not (done_reading ()) do
    let required_capa = !offset + kBytesPerRead

    in
    if Bytes.length !buf < required_capa then begin
      let new_buf = Bytes.create required_capa in
      Bytes.blit !buf 0 new_buf 0 !offset;
      buf := new_buf
    end;

    let nbytes = Unix.read client !buf !offset kBytesPerRead in
    offset := !offset + nbytes;

    try
      let s = Bytes.to_string !buf in
      let headers_end_at = BatString.find s kEndOfHeaders in

      begin
        try
          let req_line, headers = String.sub s 0 headers_end_at
          |> BatString.split ~by:"\r\n"

          in
          let req_line_tokens = BatString.nsplit ~by: " " req_line 

          in begin
            match req_line_tokens with
            | meth :: path :: http_ver :: [] ->
                let meth    = Http_req_meth.of_string meth
                and headers = Headers.of_string headers

                in
                let content_length = Headers.get_header "Content-Length" headers

                in
                let body = match content_length with
                           | None -> None
                           | Some content_length' ->
                               (* TODO: Raise a specialized exception if the content length.
                                * Or if the body is too large. *)

                               let content_length = int_of_string content_length' in
                               let body_starts_at = headers_end_at + ("\r\n\r\n"
                                                                     |> Bytes.of_string
                                                                     |> Bytes.length) in

                               let num_already_read = min content_length (Bytes.length !buf - body_starts_at) in
                               let num_to_read = max 0 (content_length - num_already_read)
                               and body_buf = Bytes.create content_length
                               in

                               Bytes.blit !buf body_starts_at body_buf 0 num_already_read;
                               ignore (Unix.read client body_buf num_already_read num_to_read);

                               Some body_buf

                in
                result := Some ({ meth; path; headers; http_ver; body })

            | _ -> raise MalformedHttpRequestError
          end
        with Not_found -> ()
      end
    with Not_found -> ()
  done;

  match !result with
  | None -> raise MalformedHttpRequestError
  | Some req -> req


and to_string (req : t) : string =
  match req with { meth; path; http_ver; headers; body; _ } ->
    let meth = Http_req_meth.to_string meth
    and headers = Headers.to_string headers
    and body = match body with None -> "" | Some b -> Bytes.to_string b
    in meth ^ " " ^ path ^ " " ^ http_ver ^ "\r\n" ^ headers ^ "\r\n" ^ body;; 
  
