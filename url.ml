type t = {
  href        : string;
  origin      : string;
  protocol    : Protocol.t;
  path        : string;
  querystring : string option;
};;

exception MalformedUrl;;

let parse (url : string) : t =
  let kProtoSep = "://" in

  (* todo: validate the url *)

  let href = url in

  let url_without_query, querystring =
    try
      let u, qs = BatString.split ~by:"?" url
      in u, Some qs
    with Not_found -> url, None

  in 
  let protostring, _ =
    try
      BatString.split ~by:kProtoSep url
    with Not_found -> raise MalformedUrl

  in
  let protocol =
      match protostring with
      | "https" -> Protocol.Https
      | "http" -> Protocol.Http
      | _ -> Other protostring

  in
  let origin, path =
    try
      let path_starts_at =
        BatString.find_from url_without_query
                            (String.length protostring + String.length kProtoSep)
                            "/"
      in
      let path_len = String.length url_without_query - path_starts_at in
      let origin = String.sub url_without_query 0 path_starts_at in
      let path = String.sub url_without_query path_starts_at path_len
      in origin, path
    with Not_found -> url_without_query, "/"

  in { href; origin; protocol; path; querystring };;

