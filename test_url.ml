open OUnit2;;


let string_of_url_querystring = function
| None -> "None"
| Some url -> "Some(\"" ^ url ^ "\")";;


let suite =
  "URL" >::: [
    "path" >:: (fun _ ->
      let { Url.path; _ } = Url.parse "https://google.com/foo"
      in assert_equal path "/foo" ~printer:Fun.id
    );

    "path with trailing slash" >:: (fun _ ->
      let { Url.path; _ } = Url.parse "https://google.com/foo/"
      in assert_equal path "/foo/" ~printer:Fun.id
    );

    "path when the url has a query" >:: (fun _ ->
      let { Url.path; _ } = Url.parse "https://google.com/foo?bar=3"
      in assert_equal path "/foo" ~printer:Fun.id
    );

    "path when there is an empty path" >:: (fun _ ->
      let { Url.path; _ } = Url.parse "https://google.com"
      in assert_equal path "/" ~printer:Fun.id
    );

    "origin when the path and query are present" >:: (fun _ ->
      let { Url.origin; _ } = Url.parse "https://google.com/foo/bar/?q=hello&foo=world"
      in assert_equal origin "https://google.com" ~printer:Fun.id
    );

    "origin when the path is present" >:: (fun _ ->
      let { Url.origin; _ } = Url.parse "https://google.com/foo/bar/"
      in assert_equal origin "https://google.com" ~printer:Fun.id
    );

    "origin with a trailing slash" >:: (fun _ ->
      let { Url.origin; _ } = Url.parse "https://google.com/"
      in assert_equal origin "https://google.com" ~printer:Fun.id
    );

    "https protocol" >:: (fun _ ->
      let { Url.protocol; _ } = Url.parse "https://google.com/"
      in assert_equal protocol Protocol.Https ~printer:Protocol.to_string
    );

    "http protocol" >:: (fun _ ->
      let { Url.protocol; _ } = Url.parse "http://google.com/"
      in assert_equal protocol Protocol.Http ~printer:Protocol.to_string
    );

    "other protocols" >:: (fun _ ->
      let { Url.protocol; _ } = Url.parse "ftp://google.com/"
      in assert_equal protocol (Protocol.Other "ftp") ~printer:Protocol.to_string
    );

    "querystring when there is no querystring" >:: (fun _ ->
      let { Url.querystring; _ } = Url.parse "http://google.com/foo/bar"
      in assert_equal querystring None ~printer:string_of_url_querystring
    );

    "querystring when there is a querystring" >:: (fun _ ->
      let { Url.querystring; _ } = Url.parse "http://google.com/foo/bar/?foo=3&bar=5"
      in assert_equal querystring (Some "foo=3&bar=5") ~printer:string_of_url_querystring
    );

    "querystring when the url has multiple question marks" >:: (fun _ ->
      let { Url.querystring; _ } = Url.parse "https://google.com?foo=3?foo=5&bar=7"

      in assert_equal querystring (Some "foo=3?foo=5&bar=7")
                      ~printer:string_of_url_querystring
    )
  ];;

