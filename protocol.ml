type t = Https | Http | Other of string;;

let to_string = function
| Https -> "https"
| Http -> "http"
| Other proto -> proto;;

