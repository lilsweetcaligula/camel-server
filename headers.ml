type t = string List.t;;


let rec of_string (s : string) : t =
  s |> BatString.nsplit ~by:"\r\n"
    |> List.filter ((!=) "")


and to_string (h : t) : string =
  (match h with [] -> "" | _ -> (String.concat "\r\n" h)) ^ "\r\n"


and empty () : t = []


and add_header (k : string) (v : string) (headers : t) : t =
  headers @ ((make_header k v) :: [])


and make_header (k : string) (v : string) : string =
  k ^ ": " ^ v


and get_header (k : string) (headers : t) : string option =
  match
    headers
    |> List.map (fun header -> BatString.split ~by:": " header)
    |> List.find_opt (fun (k', _) -> BatString.icompare k k' = 0)
  with None -> None | Some (_, v) -> Some v;;

