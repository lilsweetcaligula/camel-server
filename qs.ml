type value_t = Qs_value of string | Qs_list of string list;;

type t = (string, value_t) Hashtbl.t;;

let parse (qs : string) : t =
  let h = Hashtbl.create ~random:false 4
  and qs = if BatString.starts_with qs "?"
           then String.sub qs 1 (String.length qs - 1)
           else qs

  in
  match String.split_on_char '&' qs with
  | [] -> h
  | kvs ->
      kvs
      |> List.map (BatString.split ~by:"=")
      |> List.fold_left (fun acc (k, v) ->
          let is_list_item = BatString.ends_with k "[]"

          in
          let () =
            if is_list_item
            then
              let k' = String.sub k 0 (String.length k - 2)

              in
              let new_list = match Hashtbl.find_opt acc k' with
                             | None | Some (Qs_value _) -> v :: [] 
                             | Some (Qs_list xs) -> xs @ [v]

              in Hashtbl.replace acc k' (Qs_list new_list)
            else
              Hashtbl.replace acc k (Qs_value v)

          in acc
        ) h


and get (k : string) (q : t) : value_t option =
  Hashtbl.find_opt q k;;

