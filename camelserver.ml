let kMaxBacklog = 10;;
let kServAddr = "0.0.0.0";;
let kServDefaultPort = 8000;;

let rec app () =
  let port = if Array.length Sys.argv > 1
             then int_of_string (Sys.argv.(1))
             else kServDefaultPort

  in
  let sock = Unix.(socket ~cloexec:true PF_INET SOCK_STREAM 0) in

  Sys.set_signal Sys.sigint
    (Sys.Signal_handle (fun _signum -> Unix.close sock; exit 0));

  let () =
    try
      let addr = Unix.(ADDR_INET (inet_addr_of_string kServAddr, port)) in

      Unix.bind sock addr;
      Unix.listen sock kMaxBacklog;

      print_endline ("Listening on " ^ kServAddr ^ ":"
                                     ^ (string_of_int port) ^ "...");

      while true do
        let (client, client_addr) = Unix.accept ~cloexec:true sock in
        let request = Http_req.read client in

        print_endline (Http_req.to_string request);

        respond_not_found (client, client_addr) request;

        Unix.close client
      done
    with err ->
      Unix.close sock; raise err

  in Unix.close sock


and respond_not_found (client, _client_addr) _req =
  let headers = Headers.empty()
                |> Headers.add_header "Content-Type" "application/json"

  and body = Bytes.of_string "{ \"message\": \"not found\" }\n"

  in
  let body_len = Bytes.length body

  in
  Http_resp.respond ~status:Http_status.NotFound
                    ~headers
                    ~body
                    ~body_len
                    client;;

app ();;

