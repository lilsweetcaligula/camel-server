let kMaxBacklog = 10;;
let kServAddr = "0.0.0.0";;
let kServPort = 8000;;

let sock = Unix.(socket ~cloexec:true PF_INET SOCK_STREAM 0) in
try
  let addr = Unix.(ADDR_INET (inet_addr_of_string kServAddr, kServPort)) in

  Unix.bind sock addr;
  Unix.listen sock kMaxBacklog;

  let (client, _client_addr) = Unix.accept ~cloexec:true sock in
  let response = Bytes.of_string "Hello world!" in

  ignore (Unix.write client response 0 (Bytes.length response));
  Unix.close client;
  Unix.close sock;
with err ->
  Unix.close sock; raise err;;
