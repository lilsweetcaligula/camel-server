let kMaxBacklog = 10;;
let kServAddr = "0.0.0.0";;
let kServPort = 8000;;
let kBufLen = 32;;

let sock = Unix.(socket ~cloexec:true PF_INET SOCK_STREAM 0) in
try
  let addr = Unix.(ADDR_INET (inet_addr_of_string kServAddr, kServPort)) in

  Unix.bind sock addr;
  Unix.listen sock kMaxBacklog;

  print_endline ("Listening on " ^ kServAddr ^ ":"
                                 ^ (string_of_int kServPort) ^ "...");

  let (client, _client_addr) = Unix.accept ~cloexec:true sock in
  let buffer = Bytes.create kBufLen in
  let done_reading = ref false in

  while not !done_reading do
    let num_bytes = Unix.(read client buffer 0 (Bytes.length buffer)) in

    if num_bytes > 0 then
      ignore (Unix.write client buffer 0 num_bytes)
    else
      done_reading := true
  done;

  Unix.close client;
  Unix.close sock;
with err ->
  Unix.close sock; raise err;;
