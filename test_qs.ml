open OUnit2;;

let string_of_qvalue = function
| None -> "None"
| Some (Qs.Qs_list vs) -> vs
                       |> List.map (fun v -> "\"" ^ v ^ "\"")
                       |> String.concat "; "
                       |> Printf.sprintf "Some(Qs_list [%s])"
| Some (Qs.Qs_value v) -> Printf.sprintf "Some(Qs_value \"%s\")" v;;

let suite =
  "query" >::: [
    "querystring with a single simple value" >:: (fun _ ->
      let query = Qs.parse "a=3"

      in assert_equal (Some (Qs_value "3")) (Qs.get "a" query)
                      ~printer:string_of_qvalue);


    "querystring with a leading question mark" >:: (fun _ ->
      let query = Qs.parse "?a=3"

      in assert_equal (Some (Qs_value "3")) (Qs.get "a" query)
                      ~printer:string_of_qvalue);


    "querystring with multiple question marks" >:: (fun _ ->
      let query = Qs.parse "?foo=3?foo=5&bar=7"

      in assert_equal (Qs.get "foo" query) (Some (Qs_value "3?foo=5"))
                      ~printer:string_of_qvalue);


    "querystring of simple values" >:: (fun _ ->
      let query = Qs.parse "a=3&b=foo&c=7"

      in assert_equal (Qs.get "a" query) (Some (Qs_value "3"))
                      ~printer:string_of_qvalue);


    "querystring of simple values" >:: (fun _ ->
      let query = Qs.parse "a=3&b=foo&c=7"

      in assert_equal (Qs.get "b" query) (Some (Qs_value "foo"))
                      ~printer:string_of_qvalue);


    "querystring of simple values" >:: (fun _ ->
      let query = Qs.parse "a=3&b=foo&c=7"

      in assert_equal (Qs.get "c" query) (Some (Qs_value "7"))
                      ~printer:string_of_qvalue);


    "when a parameter does not exist" >:: (fun _ ->
      let query = Qs.parse "a=3&b=foo&c=7"

      in assert_equal (Qs.get "z" query) None
                      ~printer:string_of_qvalue);


    "querystring with a list" >:: (fun _ ->
      let query = Qs.parse "cars[]=ferrari&cars[]=lamborghini"

      in assert_equal (Qs.get "cars" query)
                      (Some (Qs_list ["ferrari"; "lamborghini"]))
                      ~printer:string_of_qvalue);


    "querystring with both simple values and a list" >:: (fun _ ->
      let query = Qs.parse "cars[]=ferrari&foo=37&cars[]=lamborghini"

      in assert_equal (Qs.get "cars" query)
                      (Some (Qs_list ["ferrari"; "lamborghini"]))
                      ~printer:string_of_qvalue);


    "querystring with both simple values and a list" >:: (fun _ ->
      let query = Qs.parse "cars[]=ferrari&foo=37&cars[]=lamborghini"

      in assert_equal (Qs.get "foo" query)
                      (Some (Qs_value "37"))
                      ~printer:string_of_qvalue);
  ];;


